#!/bin/bash

aws --region us-east-1 cloudformation create-stack \
  --template-body file://stack_template.json \
  --stack-name management \
  --capabilities CAPABILITY_IAM \
  --disable-rollback
